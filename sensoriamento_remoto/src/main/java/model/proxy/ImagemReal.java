/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.proxy;

import java.awt.image.BufferedImage;
import presenter.ImageHelper;

/**
 *
 * @author Lucas Carvalho
 */
public class ImagemReal implements IImagem{

    private final String nomeArquivo;
    private BufferedImage imagem;

    public ImagemReal(String nomeArquivo) throws Exception {
        this.nomeArquivo = nomeArquivo;
        carregarImagem(nomeArquivo);
    }
    
    private void carregarImagem(String nomeArquivo) throws Exception{
        this.imagem = ImageHelper.OpenImageWeb(nomeArquivo);
    }
    
    @Override
    public BufferedImage getImagem() {
        return imagem;
    }

    @Override
    public String getNome() {
        return this.nomeArquivo;
    }
    
}
