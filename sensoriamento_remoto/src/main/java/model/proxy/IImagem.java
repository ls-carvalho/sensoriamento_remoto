/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.proxy;

import java.awt.image.BufferedImage;

/**
 *
 * @author Lucas Carvalho
 */
public interface IImagem {

    public BufferedImage getImagem() throws Exception;
    
    public String getNome();

}
