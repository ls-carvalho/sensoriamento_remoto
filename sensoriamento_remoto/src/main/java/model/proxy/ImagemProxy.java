/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.proxy;

import java.awt.image.BufferedImage;

/**
 *
 * @author Lucas Carvalho
 */
public class ImagemProxy implements IImagem {

    private final String nomeArquivo;
    private ImagemReal imagemReal;

    public ImagemProxy(String nomeArquivo){
        this.nomeArquivo = nomeArquivo;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    @Override
    public BufferedImage getImagem() throws Exception {
        if(imagemReal == null){
            imagemReal = new ImagemReal(nomeArquivo);
        }
        return imagemReal.getImagem();
    }

    @Override
    public String getNome() {
        return this.nomeArquivo;
    }

}
