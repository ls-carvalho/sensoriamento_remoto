/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.imageio.ImageIO;

/**
 *
 * @author Lucas Carvalho
 */
public class ImageHelper {

    public static BufferedImage OpenImageWeb(String urlImagem) throws IOException {
        try {
            URL imageUrl = new URL(urlImagem);
            BufferedImage image;
            try (InputStream in = imageUrl.openStream()) {
                image = ImageIO.read(in);
            }
            return image;
        } catch (IOException e) {
            throw e;
        }
    }
}
