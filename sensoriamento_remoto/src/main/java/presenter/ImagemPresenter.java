/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presenter;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.border.EtchedBorder;
import model.proxy.IImagem;
import model.proxy.ImagemProxy;
import view.ImagemView;

/**
 *
 * @author Lucas Carvalho
 */
public class ImagemPresenter {

    private final ImagemView view;
    private ArrayList<String> url;
    private ArrayList<IImagem> imagens = new ArrayList();
    private IImagem imagem1;
    private IImagem imagem2;
    private IImagem imagem3;
    private IImagem imagem4;
    private IImagem imagem5;
    private final Path currentRelativePath = Paths.get("");
    private final String path = currentRelativePath.toAbsolutePath().toString();

    public ImagemPresenter(ArrayList<String> url) {
        this.url = url;
        this.view = new ImagemView();
        this.view.setLocationRelativeTo(view.getParent());
        this.view.setVisible(true);
        //carregar as imagens
        carregarImagens();
        //criar os listeners
        this.view.getImagem1().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                //EXIBE A IMAGEM NA TELA DE MAIOR RESOLUÇÃO
                ampliarImagem(imagem1);
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                //destaca quando o mouse entra na região
                view.getImagem1().setBorder(new EtchedBorder(Color.lightGray, Color.black));
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                //desfaz o destaque
                view.getImagem1().setBorder(null);
            }

            //<editor-fold defaultstate="collapsed" desc=" Listeners não utilizados ">
            @Override
            public void mousePressed(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }
            //</editor-fold>
        });
        this.view.getImagem2().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                //EXIBE A IMAGEM NA TELA DE MAIOR RESOLUÇÃO
                ampliarImagem(imagem2);
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                //destaca quando o mouse entra na região
                view.getImagem2().setBorder(new EtchedBorder(Color.lightGray, Color.black));
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                //desfaz o destaque
                view.getImagem2().setBorder(null);
            }

            //<editor-fold defaultstate="collapsed" desc=" Listeners não utilizados ">
            @Override
            public void mousePressed(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }
            //</editor-fold>
        });
        this.view.getImagem3().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                //EXIBE A IMAGEM NA TELA DE MAIOR RESOLUÇÃO
                ampliarImagem(imagem3);
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                //destaca quando o mouse entra na região
                view.getImagem3().setBorder(new EtchedBorder(Color.lightGray, Color.black));
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                //desfaz o destaque
                view.getImagem3().setBorder(null);
            }

            //<editor-fold defaultstate="collapsed" desc=" Listeners não utilizados ">
            @Override
            public void mousePressed(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }
            //</editor-fold>
        });
        this.view.getImagem4().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                //EXIBE A IMAGEM NA TELA DE MAIOR RESOLUÇÃO
                ampliarImagem(imagem4);
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                //destaca quando o mouse entra na região
                view.getImagem4().setBorder(new EtchedBorder(Color.lightGray, Color.black));
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                //desfaz o destaque
                view.getImagem4().setBorder(null);
            }

            //<editor-fold defaultstate="collapsed" desc=" Listeners não utilizados ">
            @Override
            public void mousePressed(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }
            //</editor-fold>
        });
        this.view.getImagem5().addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                //EXIBE A IMAGEM NA TELA DE MAIOR RESOLUÇÃO
                ampliarImagem(imagem5);
            }

            @Override
            public void mouseEntered(MouseEvent arg0) {
                //destaca quando o mouse entra na região
                view.getImagem5().setBorder(new EtchedBorder(Color.lightGray, Color.black));
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                //desfaz o destaque
                view.getImagem5().setBorder(null);
            }

            //<editor-fold defaultstate="collapsed" desc=" Listeners não utilizados ">
            @Override
            public void mousePressed(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                //não faz nada, mas tem que implementar pq herda uma interface
            }
            //</editor-fold>
        });
    }

    private void ampliarImagem(IImagem imagem) {
        try {
            this.view.getImagemReal().setIcon(new ImageIcon(new ImageIcon(imagem.getImagem()).getImage().getScaledInstance(this.view.getImagemReal().getWidth(), this.view.getImagemReal().getHeight(), Image.SCALE_SMOOTH)));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(view, e.getMessage());
        }
    }

    private void carregarImagens() {
        try {
            this.imagem1 = new ImagemProxy(this.url.get(0));
            this.view.getImagem1().setIcon(new ImageIcon(new ImageIcon(this.imagem1.getImagem()).getImage().getScaledInstance(this.view.getImagem1().getWidth(), this.view.getImagem1().getHeight(), Image.SCALE_SMOOTH)));
            this.view.getImagem1label().setText(this.imagem1.getNome());
            this.imagem2 = new ImagemProxy(this.url.get(1));
            this.view.getImagem2().setIcon(new ImageIcon(new ImageIcon(this.imagem2.getImagem()).getImage().getScaledInstance(this.view.getImagem2().getWidth(), this.view.getImagem2().getHeight(), Image.SCALE_SMOOTH)));
            this.view.getImagem2label().setText(this.imagem2.getNome());
            this.imagem3 = new ImagemProxy(this.url.get(2));
            this.view.getImagem3().setIcon(new ImageIcon(new ImageIcon(this.imagem3.getImagem()).getImage().getScaledInstance(this.view.getImagem3().getWidth(), this.view.getImagem3().getHeight(), Image.SCALE_SMOOTH)));
            this.view.getImagem3label().setText(this.imagem3.getNome());
            this.imagem4 = new ImagemProxy(this.url.get(3));
            this.view.getImagem4().setIcon(new ImageIcon(new ImageIcon(this.imagem4.getImagem()).getImage().getScaledInstance(this.view.getImagem4().getWidth(), this.view.getImagem4().getHeight(), Image.SCALE_SMOOTH)));
            this.view.getImagem4label().setText(this.imagem4.getNome());
            this.imagem5 = new ImagemProxy(this.url.get(4));
            this.view.getImagem5().setIcon(new ImageIcon(new ImageIcon(this.imagem5.getImagem()).getImage().getScaledInstance(this.view.getImagem5().getWidth(), this.view.getImagem5().getHeight(), Image.SCALE_SMOOTH)));
            this.view.getImagem5label().setText(this.imagem5.getNome());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(view, ex.getMessage());
        }
    }

}
